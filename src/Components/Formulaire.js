import React from 'react';
import Form from 'react-bootstrap/Form';
import Button from 'react-bootstrap/Button';
import { useState } from 'react';

const Formulaire = () => {
    const  [nom, setNom] = useState("");
    const  [email, setEmail]=useState("");
    const  [message , setMessage] = useState("");

   function submit(e){
    e.preventDefault()
    console.log(nom, email, message)
   }

    return (
        <div>
             <Form>
                <Form.Group className="mb-3" controlId="exampleForm.ControlInput1">
                    <Form.Label>Nom</Form.Label>
                    <Form.Control type="text" placeholder="votre nom svp" value = {nom} onChange = {(e)=>setNom(e.target.value)} />
                    <Form.Group className="mb-3" controlId="exampleForm.ControlInput1">
                    <Form.Label>Email address</Form.Label>
                    <Form.Control type="email" placeholder="yao@gmail.com" value = {email}  onChange = {(e)=>setEmail(e.target.value)}/>
                </Form.Group>
                <Form.Group className="mb-3" controlId="exampleForm.ControlTextarea1">
                    <Form.Label>message</Form.Label>
                    <Form.Control as="textarea" rows={3} value = {message} onChange = {(e)=>setMessage(e.target.value)} />
                </Form.Group>
                </Form.Group>
                <Button variant="primary" type="submit" onClick={submit}>
                    envoyer
                </Button>
            </Form>

            <div>
                <div>
                    Nom : {nom} 
                </div>
                <div>
                    Email : {email}    
                </div>
                <div>
                    Message : {message}  
                </div>
            </div>
        </div>
    );
}

export default Formulaire;
