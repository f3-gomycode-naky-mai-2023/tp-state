import logo from './logo.svg';
import './App.css';
import Formulaire from './Components/Formulaire';


function App() {
  return (
    <div className="App">
      <Formulaire></Formulaire>
    </div>
  );
}

export default App;
